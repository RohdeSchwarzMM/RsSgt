Date
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SYSTem:DATE:LOCal
	single: SYSTem:DATE:UTC

.. code-block:: python

	SYSTem:DATE:LOCal
	SYSTem:DATE:UTC



.. autoclass:: RsSgt.Implementations.System.Date.DateCls
	:members:
	:undoc-members:
	:noindex: