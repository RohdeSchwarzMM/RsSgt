Sconfiguration
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SCONfiguration:MODE

.. code-block:: python

	SCONfiguration:MODE



.. autoclass:: RsSgt.Implementations.Sconfiguration.SconfigurationCls
	:members:
	:undoc-members:
	:noindex: