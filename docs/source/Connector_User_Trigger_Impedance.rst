Impedance
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONNector:USER<CH>:TRIGger:IMPedance

.. code-block:: python

	CONNector:USER<CH>:TRIGger:IMPedance



.. autoclass:: RsSgt.Implementations.Connector.User.Trigger.Impedance.ImpedanceCls
	:members:
	:undoc-members:
	:noindex: