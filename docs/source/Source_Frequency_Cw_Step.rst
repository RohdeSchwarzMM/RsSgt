Step
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:FREQuency:[CW]:STEP:MODE
	single: [SOURce<HW>]:FREQuency:[CW]:STEP:[INCRement]

.. code-block:: python

	[SOURce<HW>]:FREQuency:[CW]:STEP:MODE
	[SOURce<HW>]:FREQuency:[CW]:STEP:[INCRement]



.. autoclass:: RsSgt.Implementations.Source.Frequency.Cw.Step.StepCls
	:members:
	:undoc-members:
	:noindex: