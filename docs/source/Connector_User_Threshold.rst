Threshold
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONNector:USER<CH>:THReshold

.. code-block:: python

	CONNector:USER<CH>:THReshold



.. autoclass:: RsSgt.Implementations.Connector.User.Threshold.ThresholdCls
	:members:
	:undoc-members:
	:noindex: