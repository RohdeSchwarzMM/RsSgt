Queue
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:QUEue:[NEXT]

.. code-block:: python

	STATus:QUEue:[NEXT]



.. autoclass:: RsSgt.Implementations.Status.Queue.QueueCls
	:members:
	:undoc-members:
	:noindex: