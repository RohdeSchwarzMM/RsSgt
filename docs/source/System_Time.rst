Time
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SYSTem:TIME:LOCal
	single: SYSTem:TIME:UTC

.. code-block:: python

	SYSTem:TIME:LOCal
	SYSTem:TIME:UTC



.. autoclass:: RsSgt.Implementations.System.Time.TimeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.system.time.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Time_DaylightSavingTime.rst
	System_Time_HrTimer.rst