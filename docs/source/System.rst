System
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SYSTem:DID
	single: SYSTem:PRESet
	single: SYSTem:PRESet:ALL
	single: SYSTem:PRESet:BASE
	single: SYSTem:RESet
	single: SYSTem:RESet:ALL
	single: SYSTem:RESet:BASE
	single: SYSTem:SREStore
	single: SYSTem:SSAVe
	single: SYSTem:TZONe
	single: SYSTem:VERSion

.. code-block:: python

	SYSTem:DID
	SYSTem:PRESet
	SYSTem:PRESet:ALL
	SYSTem:PRESet:BASE
	SYSTem:RESet
	SYSTem:RESet:ALL
	SYSTem:RESet:BASE
	SYSTem:SREStore
	SYSTem:SSAVe
	SYSTem:TZONe
	SYSTem:VERSion



.. autoclass:: RsSgt.Implementations.System.SystemCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.system.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Date.rst
	System_Device.rst
	System_DeviceFootprint.rst
	System_Error.rst
	System_Fpreset.rst
	System_Help.rst
	System_Lock.rst
	System_Time.rst