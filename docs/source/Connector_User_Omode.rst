Omode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONNector:USER<CH>:OMODe

.. code-block:: python

	CONNector:USER<CH>:OMODe



.. autoclass:: RsSgt.Implementations.Connector.User.Omode.OmodeCls
	:members:
	:undoc-members:
	:noindex: