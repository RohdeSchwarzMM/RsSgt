Magnitude
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce]:BB:IMPairment:IQOutput<CH>:IQRatio:[MAGNitude]

.. code-block:: python

	[SOURce]:BB:IMPairment:IQOutput<CH>:IQRatio:[MAGNitude]



.. autoclass:: RsSgt.Implementations.Source.Bb.Impairment.IqOutput.IqRatio.Magnitude.MagnitudeCls
	:members:
	:undoc-members:
	:noindex: