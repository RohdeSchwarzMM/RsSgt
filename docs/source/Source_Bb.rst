Bb
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:FOFFset
	single: [SOURce<HW>]:BB:POFFset

.. code-block:: python

	[SOURce<HW>]:BB:FOFFset
	[SOURce<HW>]:BB:POFFset



.. autoclass:: RsSgt.Implementations.Source.Bb.BbCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Impairment.rst