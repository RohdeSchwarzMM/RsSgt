Attenuation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:POWer:ATTenuation:DIGital

.. code-block:: python

	[SOURce<HW>]:POWer:ATTenuation:DIGital



.. autoclass:: RsSgt.Implementations.Source.Power.Attenuation.AttenuationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.power.attenuation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Power_Attenuation_RfOff.rst
	Source_Power_Attenuation_Sover.rst