Frequency
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALibration:FREQuency:TEMPerature
	single: CALibration:FREQuency:[MEASure]

.. code-block:: python

	CALibration:FREQuency:TEMPerature
	CALibration:FREQuency:[MEASure]



.. autoclass:: RsSgt.Implementations.Calibration.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: