External
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:ROSCillator:EXTernal:FREQuency

.. code-block:: python

	[SOURce<HW>]:ROSCillator:EXTernal:FREQuency



.. autoclass:: RsSgt.Implementations.Source.Roscillator.External.ExternalCls
	:members:
	:undoc-members:
	:noindex: