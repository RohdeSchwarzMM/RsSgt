RefLo
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONNector:REFLo:OUTPut

.. code-block:: python

	CONNector:REFLo:OUTPut



.. autoclass:: RsSgt.Implementations.Connector.RefLo.RefLoCls
	:members:
	:undoc-members:
	:noindex: