Fixed
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:FREQuency:[FIXed]:RCL
	single: [SOURce<HW>]:FREQuency:[FIXed]

.. code-block:: python

	[SOURce<HW>]:FREQuency:[FIXed]:RCL
	[SOURce<HW>]:FREQuency:[FIXed]



.. autoclass:: RsSgt.Implementations.Source.Frequency.Fixed.FixedCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.frequency.fixed.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Frequency_Fixed_Step.rst