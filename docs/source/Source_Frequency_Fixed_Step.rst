Step
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:FREQuency:[FIXed]:STEP:MODE
	single: [SOURce<HW>]:FREQuency:[FIXed]:STEP:[INCRement]

.. code-block:: python

	[SOURce<HW>]:FREQuency:[FIXed]:STEP:MODE
	[SOURce<HW>]:FREQuency:[FIXed]:STEP:[INCRement]



.. autoclass:: RsSgt.Implementations.Source.Frequency.Fixed.Step.StepCls
	:members:
	:undoc-members:
	:noindex: