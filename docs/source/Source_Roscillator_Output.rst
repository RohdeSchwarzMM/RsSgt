Output
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:ROSCillator:OUTPut:FREQuency

.. code-block:: python

	[SOURce<HW>]:ROSCillator:OUTPut:FREQuency



.. autoclass:: RsSgt.Implementations.Source.Roscillator.Output.OutputCls
	:members:
	:undoc-members:
	:noindex: