Enums
=========

AttMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AttMode.APASsive
	# All values (3x):
	APASsive | AUTO | FIXed

BbSystemConfiguration
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BbSystemConfiguration.AFETracking
	# All values (2x):
	AFETracking | STANdard

CalPowDetAtt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CalPowDetAtt.HIGH
	# All values (4x):
	HIGH | LOW | MED | OFF

FreqStepMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FreqStepMode.DECimal
	# All values (2x):
	DECimal | USER

ImpG50G10K
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ImpG50G10K.G10K
	# All values (2x):
	G10K | G50

InclExcl
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.InclExcl.EXCLude
	# All values (2x):
	EXCLude | INCLude

InputImpRf
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.InputImpRf.G10K
	# All values (3x):
	G10K | G1K | G50

IqMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IqMode.ANALog
	# All values (2x):
	ANALog | BASeband

OpMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.OpMode.BBBYpass
	# All values (2x):
	BBBYpass | NORMal

PowAlcState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowAlcState._1
	# All values (4x):
	_1 | AUTO | OFF | ON

PowLevBehaviour
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowLevBehaviour.AUTO
	# All values (6x):
	AUTO | CVSWr | CWSWr | MONotone | UNINterrupted | USER

PowLevMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowLevMode.LOWDistortion
	# All values (4x):
	LOWDistortion | LOWNoise | NORMal | USER

PowOutpPonMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowOutpPonMode.OFF
	# All values (2x):
	OFF | UNCHanged

PowRfOffMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowRfOffMode.FIXed
	# All values (2x):
	FIXed | MAX

PowSensWithUndef
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.PowSensWithUndef.SENS1
	# Last value:
	value = enums.PowSensWithUndef.UNDefined
	# All values (9x):
	SENS1 | SENS2 | SENS3 | SENS4 | SENSor1 | SENSor2 | SENSor3 | SENSor4
	UNDefined

RefLoOutput
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RefLoOutput.LO
	# All values (3x):
	LO | OFF | REF

RoscFreqExt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RoscFreqExt._1000MHZ
	# All values (4x):
	_1000MHZ | _100MHZ | _10MHZ | _13MHZ

SlopeType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SlopeType.NEGative
	# All values (2x):
	NEGative | POSitive

SourceInt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SourceInt.EXTernal
	# All values (2x):
	EXTernal | INTernal

Test
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Test._0
	# All values (4x):
	_0 | _1 | RUNning | STOPped

UserPlug
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.UserPlug.CIN
	# Last value:
	value = enums.UserPlug.TRIGger
	# All values (18x):
	CIN | COUT | HIGH | LOW | MARRived | MKR1 | MKR2 | MLATency
	NEXT | PEMSource | PETRigger | PVOut | SIN | SNValid | SOUT | SVALid
	TOUT | TRIGger

