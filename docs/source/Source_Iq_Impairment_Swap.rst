Swap
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:IQ:IMPairment:SWAP:[STATe]

.. code-block:: python

	[SOURce<HW>]:IQ:IMPairment:SWAP:[STATe]



.. autoclass:: RsSgt.Implementations.Source.Iq.Impairment.Swap.SwapCls
	:members:
	:undoc-members:
	:noindex: