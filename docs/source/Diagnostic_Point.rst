Point
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:POINt:CATalog

.. code-block:: python

	DIAGnostic:POINt:CATalog



.. autoclass:: RsSgt.Implementations.Diagnostic.Point.PointCls
	:members:
	:undoc-members:
	:noindex: