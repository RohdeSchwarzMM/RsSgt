SymbolRate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BBIN:SRATe:[ACTual]

.. code-block:: python

	[SOURce<HW>]:BBIN:SRATe:[ACTual]



.. autoclass:: RsSgt.Implementations.Source.Bbin.SymbolRate.SymbolRateCls
	:members:
	:undoc-members:
	:noindex: