State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce]:BB:IMPairment:IQOutput<CH>:STATe

.. code-block:: python

	[SOURce]:BB:IMPairment:IQOutput<CH>:STATe



.. autoclass:: RsSgt.Implementations.Source.Bb.Impairment.IqOutput.State.StateCls
	:members:
	:undoc-members:
	:noindex: