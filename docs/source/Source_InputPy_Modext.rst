Modext
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce]:INPut:MODext:IMPedance

.. code-block:: python

	[SOURce]:INPut:MODext:IMPedance



.. autoclass:: RsSgt.Implementations.Source.InputPy.Modext.ModextCls
	:members:
	:undoc-members:
	:noindex: