Slope
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONNector:USER<CH>:TRIGger:SLOPe

.. code-block:: python

	CONNector:USER<CH>:TRIGger:SLOPe



.. autoclass:: RsSgt.Implementations.Connector.User.Trigger.Slope.SlopeCls
	:members:
	:undoc-members:
	:noindex: