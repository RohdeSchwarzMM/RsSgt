Bband
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALibration:IQModulator:BBANd:[STATe]

.. code-block:: python

	CALibration:IQModulator:BBANd:[STATe]



.. autoclass:: RsSgt.Implementations.Calibration.IqModulator.Bband.BbandCls
	:members:
	:undoc-members:
	:noindex: