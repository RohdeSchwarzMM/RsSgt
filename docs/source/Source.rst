Source
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:OPMode

.. code-block:: python

	[SOURce<HW>]:OPMode



.. autoclass:: RsSgt.Implementations.Source.SourceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb.rst
	Source_Bbin.rst
	Source_Frequency.rst
	Source_InputPy.rst
	Source_Iq.rst
	Source_Loscillator.rst
	Source_Phase.rst
	Source_Power.rst
	Source_Roscillator.rst