Trigger
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:INPut:TRIGger:IMPedance
	single: [SOURce<HW>]:INPut:TRIGger:LEVel

.. code-block:: python

	[SOURce<HW>]:INPut:TRIGger:IMPedance
	[SOURce<HW>]:INPut:TRIGger:LEVel



.. autoclass:: RsSgt.Implementations.Source.InputPy.Trigger.TriggerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.inputPy.trigger.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_InputPy_Trigger_Bband.rst