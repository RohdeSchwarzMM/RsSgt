Cw
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:FREQuency:[CW]:RCL
	single: [SOURce<HW>]:FREQuency:[CW]

.. code-block:: python

	[SOURce<HW>]:FREQuency:[CW]:RCL
	[SOURce<HW>]:FREQuency:[CW]



.. autoclass:: RsSgt.Implementations.Source.Frequency.Cw.CwCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.frequency.cw.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Frequency_Cw_Step.rst