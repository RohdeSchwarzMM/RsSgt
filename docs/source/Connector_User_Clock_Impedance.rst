Impedance
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONNector:USER<CH>:CLOCk:IMPedance

.. code-block:: python

	CONNector:USER<CH>:CLOCk:IMPedance



.. autoclass:: RsSgt.Implementations.Connector.User.Clock.Impedance.ImpedanceCls
	:members:
	:undoc-members:
	:noindex: