Sensor
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:POWer:SERVoing:SENSor:APERture
	single: [SOURce<HW>]:POWer:SERVoing:SENSor

.. code-block:: python

	[SOURce<HW>]:POWer:SERVoing:SENSor:APERture
	[SOURce<HW>]:POWer:SERVoing:SENSor



.. autoclass:: RsSgt.Implementations.Source.Power.Servoing.Sensor.SensorCls
	:members:
	:undoc-members:
	:noindex: