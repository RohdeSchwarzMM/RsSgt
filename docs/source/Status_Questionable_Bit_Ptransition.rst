Ptransition
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:QUEStionable:BIT<BITNR>:PTRansition

.. code-block:: python

	STATus:QUEStionable:BIT<BITNR>:PTRansition



.. autoclass:: RsSgt.Implementations.Status.Questionable.Bit.Ptransition.PtransitionCls
	:members:
	:undoc-members:
	:noindex: