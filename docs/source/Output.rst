Output
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: OUTPut<HW>:AMODe

.. code-block:: python

	OUTPut<HW>:AMODe



.. autoclass:: RsSgt.Implementations.Output.OutputCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.output.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Output_Afixed.rst
	Output_Protection.rst
	Output_State.rst