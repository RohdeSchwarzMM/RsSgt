IqModulator
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALibration:IQModulator:FULL
	single: CALibration:IQModulator:LOCal
	single: CALibration:IQModulator:TEMPerature

.. code-block:: python

	CALibration:IQModulator:FULL
	CALibration:IQModulator:LOCal
	CALibration:IQModulator:TEMPerature



.. autoclass:: RsSgt.Implementations.Calibration.IqModulator.IqModulatorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.calibration.iqModulator.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Calibration_IqModulator_Bband.rst
	Calibration_IqModulator_IqModulator.rst