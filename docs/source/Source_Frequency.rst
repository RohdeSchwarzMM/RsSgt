Frequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce]:FREQuency:OFFSet

.. code-block:: python

	[SOURce]:FREQuency:OFFSet



.. autoclass:: RsSgt.Implementations.Source.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.frequency.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Frequency_Cw.rst
	Source_Frequency_Fixed.rst