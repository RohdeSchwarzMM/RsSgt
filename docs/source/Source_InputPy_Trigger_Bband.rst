Bband
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:INPut:TRIGger:[BBANd]:SLOPe

.. code-block:: python

	[SOURce<HW>]:INPut:TRIGger:[BBANd]:SLOPe



.. autoclass:: RsSgt.Implementations.Source.InputPy.Trigger.Bband.BbandCls
	:members:
	:undoc-members:
	:noindex: