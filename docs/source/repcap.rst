RepCaps
=========

HwInstance (Global)
----------------------------------------------------

.. code-block:: python

	# Setting:
	driver.repcap_hwInstance_set(repcap.HwInstance.InstA)
	# Range:
	InstA .. InstH
	# All values (8x):
	InstA | InstB | InstC | InstD | InstE | InstF | InstG | InstH

BitNumberNull
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.BitNumberNull.Nr0
	# Range:
	Nr0 .. Nr15
	# All values (16x):
	Nr0 | Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7
	Nr8 | Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15

IqConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.IqConnector.Nr1
	# Values (4x):
	Nr1 | Nr2 | Nr3 | Nr4

UserIx
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.UserIx.Nr1
	# Range:
	Nr1 .. Nr48
	# All values (48x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16
	Nr17 | Nr18 | Nr19 | Nr20 | Nr21 | Nr22 | Nr23 | Nr24
	Nr25 | Nr26 | Nr27 | Nr28 | Nr29 | Nr30 | Nr31 | Nr32
	Nr33 | Nr34 | Nr35 | Nr36 | Nr37 | Nr38 | Nr39 | Nr40
	Nr41 | Nr42 | Nr43 | Nr44 | Nr45 | Nr46 | Nr47 | Nr48

