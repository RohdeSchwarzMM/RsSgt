Bbin
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BBIN:ODELay

.. code-block:: python

	[SOURce<HW>]:BBIN:ODELay



.. autoclass:: RsSgt.Implementations.Source.Bbin.BbinCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bbin.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bbin_SymbolRate.rst