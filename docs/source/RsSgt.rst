RsSgt API Structure
========================================


.. rubric:: Global RepCaps

.. code-block:: python
	
	driver = RsSgt('TCPIP::192.168.2.101::hislip0')
	# HwInstance range: InstA .. InstH
	rc = driver.repcap_hwInstance_get()
	driver.repcap_hwInstance_set(repcap.HwInstance.InstA)

.. autoclass:: RsSgt.RsSgt
	:members:
	:undoc-members:
	:noindex:

.. rubric:: Subgroups

.. toctree::
	:maxdepth: 6
	:glob:

	Calibration.rst
	Connector.rst
	Diagnostic.rst
	MassMemory.rst
	Output.rst
	Sconfiguration.rst
	Source.rst
	Status.rst
	System.rst
	Test.rst