Keyboard
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TEST:KEYBoard:[STATe]

.. code-block:: python

	TEST:KEYBoard:[STATe]



.. autoclass:: RsSgt.Implementations.Test.Keyboard.KeyboardCls
	:members:
	:undoc-members:
	:noindex: