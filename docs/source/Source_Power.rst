Power
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:POWer:LMODe
	single: [SOURce<HW>]:POWer:PEP
	single: [SOURce<HW>]:POWer:POWer
	single: [SOURce<HW>]:POWer:SCHaracteristic

.. code-block:: python

	[SOURce<HW>]:POWer:LMODe
	[SOURce<HW>]:POWer:PEP
	[SOURce<HW>]:POWer:POWer
	[SOURce<HW>]:POWer:SCHaracteristic



.. autoclass:: RsSgt.Implementations.Source.Power.PowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.power.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Power_Alc.rst
	Source_Power_Attenuation.rst
	Source_Power_Level.rst
	Source_Power_Limit.rst
	Source_Power_Range.rst
	Source_Power_Servoing.rst