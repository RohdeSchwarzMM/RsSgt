Loscillator
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:LOSCillator:SOURce

.. code-block:: python

	[SOURce<HW>]:LOSCillator:SOURce



.. autoclass:: RsSgt.Implementations.Source.Loscillator.LoscillatorCls
	:members:
	:undoc-members:
	:noindex: