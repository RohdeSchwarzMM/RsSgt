Trigger
----------------------------------------





.. autoclass:: RsSgt.Implementations.Connector.User.Trigger.TriggerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.connector.user.trigger.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Connector_User_Trigger_Impedance.rst
	Connector_User_Trigger_Slope.rst